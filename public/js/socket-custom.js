var socket = io();

socket.on('connect', function() {
    console.log('Conectado al servidor');
});

socket.on('disconnect', function() {
    console.log('Se perdio la conexion con el servidor');
});

//enviar informacion
socket.emit('sendMessage', {
    user: 'Fernando',
    message: 'Hola'
}, function(res) {
    console.log('response server:', res);
});

//escuchar informacion
socket.on('sendMessage', function(message) {
    console.log('server:', message);
});
