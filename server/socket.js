const { io } = require('./app');

//client -> informacion de la computadora o conexion
io.on('connection', (client) => {
    console.log('Usuario conectado');

    client.emit('sendMessage', {
        user: 'Admin',
        message: 'Bienvenido aqui'
    });

    client.on('disconnect', () => {
        console.log('Usuario desconectado');
    });

    //escuchar client
    client.on('sendMessage', (data, callback) => {
        console.log(data);

        client.broadcast.emit('sendMessage', data);

        // if(data.user) {
        //     callback({
        //         ok: true,
        //         message: 'Salio bien'
        //     });
        // }
        // else {
        //     callback({
        //         ok: false,
        //         message: 'Salio mal'
        //     });
        // }
    });
});