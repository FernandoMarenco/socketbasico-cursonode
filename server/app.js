const express = require('express');
const socketIO = require('socket.io');
const path = require('path');
const http = require('http');

const app = express();
const server = http.createServer(app);

//public
const publicPath = path.resolve(__dirname, '../public');
app.use(express.static(publicPath));

//socket io, conexion directa con el servidor, comunicacion del backend
module.exports.io = socketIO(server);
require('./socket');

//port
const port = process.env.PORT || 3000;

server.listen(port, (error) => {
    if(error) throw Error(error);
    console.log(`Servidor corriendo en el puerto ${port}`);
})